import java.util.ArrayList;
import java.util.Date;

class DisneyDB{
    
    /*----------------------Atributos da base de dados-------------------------*/
    private int ID; // int
    private String Type; // String de tamanho fixo
    private String Title; // String de tamanho variavel
    private Date AddDate; // data
    private int ReleaseYear; // int
    private ArrayList<String> Genres;//lista de valores com separador a definir
    /*-------------------------------------------------------------------------*/
    
    /*-------------------------- Funções construtoras -------------------------*/
    
    public DisneyDB(){
        this.ID = -1;
        this.Type = "";
        this.Title = "";
        this.AddDate = null;
        this.ReleaseYear = -1;
        this.Genres = new ArrayList<String>();
    }

    public DisneyDB(int ID,String Type, String Title, Date AddDate, int ReleaseYear, ArrayList<String> Genres){
        this.ID = ID;
        this.Type = Type;
        this.Title = Title;
        this.AddDate = AddDate;
        this.ReleaseYear = ReleaseYear;
        this.Genres = Genres;
    }
    /*-------------------------------------------------------------------------*/
    
    /*-------------------- Funções de controle de atributos -------------------*/

    /*----- funções get -----*/
    public int GetID() { return ID; }
    public String GetType() { return Type; }
    public String GetTitle() { return Title; }
    public Date GetAddDate() { return AddDate; }
    public int GetReleaseYear() { return ReleaseYear; }
    public ArrayList<String> GetGenres() { return Genres; }
    /*-----------------------*/

    /*----- funções set -----*/
    public void SetID(int ID){ this.ID = ID; }
    public void SetType(String Type){this.Type = Type;}
    public void SetTitle(String Title){this.Title = Title;}
    public void SetAddDate(Date AddDate){this.AddDate = AddDate;}
    public void SetGenres(ArrayList<String> Genres){this.Genres = Genres;}
    /*-----------------------*/
    
    /*-------------------------------------------------------------------------*/

    
}